import React, { ReactElement, VFC } from "react";
import { IUser } from "types/user";

interface Props {
  user: IUser;
}

export const UserProfile: VFC<Props> = (props): ReactElement => {
  const { user } = props;
  return (
    <dl>
      <dt>名前</dt>
      <dl>{user.name}</dl>
      <td>趣味</td>
      <dl>{user.hobbies?.join(" / ")}</dl>
    </dl>
  );
};
