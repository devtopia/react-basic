import { Header } from "components/atoms/layout/Header";
import React, { ReactElement } from "react";

interface HeaderProps {
  children: JSX.Element | JSX.Element[];
}

export const HeaderOnly = (props: HeaderProps): ReactElement => {
  const { children } = props;
  return (
    <>
      <Header />
      {children}
    </>
  );
};
