import { Footer } from "components/atoms/layout/Footer";
import { Header } from "components/atoms/layout/Header";
import React, { ReactElement } from "react";

interface DefaultProps {
  children: JSX.Element | JSX.Element[];
}

export const DefaultLayout = (props: DefaultProps): ReactElement => {
  const { children } = props;
  return (
    <>
      <Header />
      {children}
      <Footer />
    </>
  );
};
