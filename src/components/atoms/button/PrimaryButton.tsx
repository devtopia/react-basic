import React, { ReactElement } from "react";
import styled from "styled-components";

import { BaseButton } from "./BaseButton";

interface LabelProps {
  children: string;
}
export const PrimaryButton = (props: LabelProps): ReactElement => {
  const { children } = props;
  return <SButton>{children}</SButton>;
};

const SButton = styled(BaseButton)`
  background-color: #40514e;
`;
