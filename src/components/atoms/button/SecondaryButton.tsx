import React, { ReactElement } from "react";
import styled from "styled-components";

import { BaseButton } from "./BaseButton";

interface ButtonProps {
  children: string;
  onClick: () => void;
}
export const SecondaryButton = (props: ButtonProps): ReactElement => {
  const { children, onClick } = props;
  return <SButton onClick={onClick}>{children}</SButton>;
};

const SButton = styled(BaseButton)`
  background-color: #11999e;
`;
