import React, { ReactElement } from "react";
import styled from "styled-components";

interface inputProps {
  placeholder: string;
}

export const Input = (props: inputProps): ReactElement => {
  const { placeholder = "" } = props;
  return <SInput type="text" placeholder={placeholder} />;
};

const SInput = styled.input`
  padding: 8px 16px;
  border: solid #ddd 1px;
  border-radius: 9999px;
  outline: none;
`;
