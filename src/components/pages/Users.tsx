import { SecondaryButton } from "components/atoms/button/SecondaryButton";
import { SearchInput } from "components/molecules/SearchInput";
import { UserCard } from "components/organisms/user/UserCard";
// import { UserContext } from "providers/UserProvider";
import React, { ReactElement } from "react";
import { useRecoilState } from "recoil";
import { userState } from "store/userState";
import styled from "styled-components";

const users = [...Array(10).keys()].map((val) => {
  return {
    id: val,
    name: `devtopia-${val}`,
    image: "https://source.unsplash.com/oU6KZTXhuvk",
    email: "devtopia@example.com",
    phone: "080-1111-2222",
    company: {
      name: "devtopia Inc."
    },
    website: "https://google.com"
  };
});

export const Users = (): ReactElement => {
  // const { userInfo, setUserInfo } = useContext(UserContext);
  const [userInfo, setUserInfo] = useRecoilState(userState);
  const onClickSwitch = (): void => {
    setUserInfo({ isAdmin: userInfo != null && !userInfo.isAdmin });
  };

  return (
    <SContainer>
      <h2>This is a users list page.</h2>
      <SearchInput />
      <br />
      <SecondaryButton onClick={onClickSwitch}>Change</SecondaryButton>
      <SUserArea>
        {users.map((user) => (
          <UserCard key={user.id} user={user} />
        ))}
      </SUserArea>
    </SContainer>
  );
};

const SContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 24px;
`;

const SUserArea = styled.div`
  padding-top: 40px;
  width: 100%;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
  grid-gap: 20px;
`;
