import { SecondaryButton } from "components/atoms/button/SecondaryButton";
// import { UserContext } from "providers/UserProvider";
import React, { ReactElement } from "react";
import { useHistory } from "react-router-dom";
import { useSetRecoilState } from "recoil";
import { userState } from "store/userState";
import styled from "styled-components";

export const Top = (): ReactElement => {
  const history = useHistory();
  // const userContext = useContext(UserContext);
  const setUserInfo = useSetRecoilState(userState);

  const onClickAdmin = (): void => {
    setUserInfo({ isAdmin: true });
    history.push("/users");
  };
  const onClickUser = (): void => {
    setUserInfo({ isAdmin: false });
    history.push("/users");
  };
  return (
    <SContainer>
      <h2>This is a Top page.</h2>
      <SecondaryButton onClick={onClickAdmin}>Administrator</SecondaryButton>
      <br />
      <br />
      <SecondaryButton onClick={onClickUser}>User</SecondaryButton>
    </SContainer>
  );
};

const SContainer = styled.div`
  text-align: center;
`;
