import React, { ReactElement, memo } from "react";

interface openProps {
  open: boolean;
  onClickClose: () => void;
}

const style = {
  width: "100%",
  height: "200px",
  backgroundColor: "khaki"
};

// memoでcomponentを囲む。
// propsが変更しない限り、再レンダリングしない。
const ChildArea = memo((props: openProps): ReactElement => {
  const { open, onClickClose } = props;
  console.log("ChildComponent is rendered.");

  const data = [...Array(2000).keys()];
  data.forEach(() => {
    console.log("...");
  });

  return (
    <>
      {open ? (
        <div style={style}>
          <p>Child Component</p>
          <button onClick={onClickClose}>Close</button>
        </div>
      ) : null}
    </>
  );
});

ChildArea.displayName = "ChildArea";
export default ChildArea;
