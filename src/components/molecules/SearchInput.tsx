import { PrimaryButton } from "components/atoms/button/PrimaryButton";
import { Input } from "components/atoms/input/input";
import React, { ReactElement, memo } from "react";
import styled from "styled-components";

export const SearchInput = memo((): ReactElement => {
  console.log("SearchInput");
  return (
    <SContainer>
      <Input placeholder="検索条件を入力" />
      <SButtonWrapper>
        <PrimaryButton>検索</PrimaryButton>
      </SButtonWrapper>
    </SContainer>
  );
});

SearchInput.displayName = "SearchInput";

const SContainer = styled.div`
  display: flex;
  align-items: center;
`;

const SButtonWrapper = styled.div`
  padding-left: 8px;
`;
