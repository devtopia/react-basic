// import { UserContext } from "providers/UserProvider";
import React, { ReactElement, memo } from "react";
import { useRecoilValue } from "recoil";
import { userState } from "store/userState";
import styled from "styled-components";

interface UserInfoProps {
  image: string;
  name: string;
}

export const UserIconWithName = memo((props: UserInfoProps): ReactElement => {
  console.log("UserIconWithName");
  const { image, name } = props;
  // const { userInfo } = useContext(UserContext);
  const userInfo = useRecoilValue(userState);
  const isAdmin = userInfo != null ? userInfo.isAdmin : false;

  return (
    <SContainer>
      <SImg height={160} width={160} src={image} alt="Profile" />
      <SName>{name}</SName>
      {isAdmin != null && isAdmin && <SEdit>Edit</SEdit>}
    </SContainer>
  );
});

UserIconWithName.displayName = "UserIconWithName";

const SContainer = styled.div`
  text-align: center;
`;
const SImg = styled.img`
  border-radius: 50%;
`;
const SName = styled.p`
  font-size: 18px;
  font-weight: bold;
  margin: 0;
  color: #40514e;
`;
const SEdit = styled.span`
  text-decoration: underline;
  color: #aaa;
  cursor: pointer;
`;
