import { Card } from "components/atoms/card/Card";
import { UserIconWithName } from "components/molecules/user/UserIconWithName";
import React, { ReactElement, memo } from "react";
import styled from "styled-components";

interface UserProps {
  user: {
    name: string;
    image: string;
    email: string;
    phone: string;
    company: {
      name: string;
    };
    website: string;
  };
}

export const UserCard = memo((props: UserProps): ReactElement => {
  console.log("UserCard");
  const { user } = props;
  return (
    <Card>
      <UserIconWithName image={user.image} name={user.name} />
      <SDl>
        <dt>Email</dt>
        <dd>{user.email}</dd>
        <dt>TEL</dt>
        <dd>{user.phone}</dd>
        <dt>Company</dt>
        <dd>{user.company.name}</dd>
        <dt>WEB</dt>
        <dd>{user.website}</dd>
      </SDl>
    </Card>
  );
});

UserCard.displayName = "UserCard";

const SDl = styled.dl`
  text-align: left;
  margin-bottom: 0px;
  dt {
    float: left;
  }
  dd {
    padding-left: 32px;
    padding-bottom: 8px;
    overflow-wrap: break-word;
  }
`;
