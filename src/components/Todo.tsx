import React, { ReactElement } from "react";
import { ITodo } from "types/todo";

export const Todo = (props: Omit<ITodo, "id">): ReactElement => {
  const { title, userId, completed = false } = props;
  const completeMark = completed ? "[完]" : "[未]";
  return <p>{`${completeMark} ${title}(userId: ${userId})`}</p>;
};
