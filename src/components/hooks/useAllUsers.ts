import axios from "axios";
import { useState } from "react";
import { IUser } from "types/api/user";

import { IUserProfile } from "../../types/userProfile";

interface IAllUsers {
  getUsers: () => void;
  userProfiles: IUserProfile[];
  loading: boolean;
  error: boolean;
}

export const useAllUsers = (): IAllUsers => {
  const [userProfiles, setUserProfiles] = useState<IUserProfile[]>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);

  const getUsers = (): void => {
    setLoading(true);
    setError(false);
    axios
      .get<IUser[]>("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        console.log(res.data);
        const data = res.data.map((user) => ({
          id: user.id,
          name: `${user.name}(${user.username})`,
          email: user.email,
          address: `${user.address.city}${user.address.suite}${user.address.street}`
        }));
        setUserProfiles(data);
      })
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return { getUsers, userProfiles, loading, error };
};
