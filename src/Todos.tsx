import { Text } from "Text";
import axios from "axios";
import { Todo } from "components/Todo";
import { UserProfile } from "components/UserProfile";
import React, { ReactElement, useState } from "react";
import { ITodo } from "types/todo";
import { IUser } from "types/user";

const user: IUser = {
  name: "devtopia",
  hobbies: ["Manga", "Anime", "Study"]
};
export const Todos = (): ReactElement => {
  const [todos, setTodos] = useState<ITodo[]>([]);

  const onClickFetchData = (): void => {
    axios
      .get<ITodo[]>("https://jsonplaceholder.typicode.com/todos")
      .then((res) => {
        setTodos(res.data);
      })
      .catch((err) => console.log(err));
  };
  return (
    <div className="App">
      <UserProfile user={user} />
      <Text color="red" fontSize="18px">
        あいうえお
      </Text>
      <button onClick={onClickFetchData}>Get data</button>
      {todos.map((todo) => (
        <Todo key={todo.id} title={todo.title} userId={todo.userId} completed={todo.completed} />
      ))}
    </div>
  );
};
