import React, { ReactElement, createContext, useState } from "react";

interface IUserInfo {
  isAdmin: boolean;
}

interface IUserContext {
  userInfo: IUserInfo | null | undefined;
  setUserInfo: React.Dispatch<React.SetStateAction<IUserInfo | null | undefined>>;
}

interface ProviderProps {
  children: React.ReactNode;
}

export const UserContext = createContext({} as IUserContext);

export const UserProvider = (props: ProviderProps): ReactElement => {
  const { children } = props;
  const [userInfo, setUserInfo] = useState<IUserInfo | null>();
  return <UserContext.Provider value={{ userInfo, setUserInfo }}>{children}</UserContext.Provider>;
};
