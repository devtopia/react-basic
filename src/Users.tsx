import { useAllUsers } from "components/hooks/useAllUsers";
import { UserCard } from "components/nocustom/UserCard";
import React, { ReactElement } from "react";

export const Users = (): ReactElement => {
  const { getUsers, userProfiles, loading, error } = useAllUsers();

  const onClickFetchUser = (): void => getUsers();
  return (
    <div>
      <button onClick={onClickFetchUser}>Get Data</button>
      <br />
      {error ? (
        <p style={{ color: "red" }}>Cannot get the data.</p>
      ) : loading ? (
        <p>Loading...</p>
      ) : (
        <>
          {userProfiles.map((user) => (
            <UserCard key={user.id} user={user} />
          ))}
        </>
      )}
    </div>
  );
};
