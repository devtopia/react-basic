import React, { FC, ReactElement } from "react";

interface IText {
  color: string;
  fontSize: string;
  children: React.ReactNode;
}

export const Text: FC<IText> = (props): ReactElement => {
  const { color, fontSize, children } = props;
  return <p style={{ color, fontSize }}>{children}</p>;
};
