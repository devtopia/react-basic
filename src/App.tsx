import React, { ReactElement, useCallback, useMemo, useState } from "react";

import "./App.css";
import ChildArea from "./components/ChildArea";

function App(): ReactElement {
  console.log("ParentComponent is rendered.");
  const [text, setText] = useState("");
  const [open, setOpen] = useState(false);

  const onChangeText = (e: React.ChangeEvent<HTMLInputElement>): void => setText(e.target.value);
  const onClickOpen = (): void => setOpen(!open);

  // Allow function is considered a new function every time.
  const onClickClose = useCallback((): void => setOpen(false), [open]);

  // 変数をmemo化する。
  // 第１引数の関数の実行に時間がかかる場合
  // memo化することによって第２引数の配列に変化がない限り
  // 関数は呼び出されない。
  const temp = useMemo(() => 1 + 3, []);
  console.log(temp);

  return (
    <div className="App">
      <input value={text} onChange={onChangeText} />
      <br />
      <br />
      <button onClick={onClickOpen}>表示</button>
      <ChildArea open={open} onClickClose={onClickClose} />
    </div>
  );
}

export default App;
