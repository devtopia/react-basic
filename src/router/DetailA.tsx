import React, { ReactElement } from "react";
import { useHistory, useLocation } from "react-router-dom";

export const DetailA = (): ReactElement => {
  const { state } = useLocation();
  console.log(state);

  const history = useHistory();
  const onClickBack = (): void => history.goBack();

  return (
    <div>
      <h1>DetailAページです。</h1>
      <button onClick={onClickBack}>戻る</button>
    </div>
  );
};
