import React from "react";

import { DetailA } from "./DetailA";
import { DetailB } from "./DetailB";
import { Page1 } from "./Page1";

export const page1Routes = [
  {
    path: "/",
    exact: true,
    children: <Page1 />
  },
  {
    path: "/detailA",
    exact: false,
    children: <DetailA />
  },
  {
    path: "/detailB",
    exact: false,
    children: <DetailB />
  }
];
