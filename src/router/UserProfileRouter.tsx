import { Top } from "components/pages/Top";
import { Users } from "components/pages/Users";
import { DefaultLayout } from "components/templates/DefaultLayout";
import { HeaderOnly } from "components/templates/HeaderOnly";
import React, { ReactElement } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

export const UserProfileRouter = (): ReactElement => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/">
          <DefaultLayout>
            <Top />
          </DefaultLayout>
        </Route>
        <Route path="/users">
          <HeaderOnly>
            <Users />
          </HeaderOnly>
        </Route>
      </Switch>
    </BrowserRouter>
  );
};
