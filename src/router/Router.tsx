import { Page404 } from "404";
import React, { ReactElement } from "react";
import { Route, Switch } from "react-router-dom";

import { Home } from "./Home";
import { page1Routes } from "./Page1Routes";
import { page2Routes } from "./Page2Routes";

export const Router = (): ReactElement => {
  return (
    <Switch>
      <Route exact path="/">
        <Home />
      </Route>
      <Route
        path="/page1"
        render={({ match }) => (
          <Switch>
            {page1Routes.map((route) => (
              <Route key={route.path} exact={route.exact} path={`${match.url}${route.path}`}>
                {route.children}
              </Route>
            ))}
          </Switch>
        )}
      />
      <Route
        path="/page2"
        render={({ match }) => (
          <Switch>
            {page2Routes.map((route) => (
              <Route key={route.path} exact={route.exact} path={`${match.url}${route.path}`}>
                {route.children}
              </Route>
            ))}
          </Switch>
        )}
      />
      <Route path="*">
        <Page404 />
      </Route>
    </Switch>
  );
};
