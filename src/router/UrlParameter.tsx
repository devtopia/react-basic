import React, { ReactElement } from "react";
import { useLocation, useParams } from "react-router-dom";

interface Params {
  id: string;
}

interface QueryParams {
  search: string;
}
console.log(location);
export const UrlParameter = (): ReactElement => {
  const { id } = useParams<Params>();
  const { search } = useLocation<QueryParams>();
  const query = new URLSearchParams(search);
  console.log(query);
  return (
    <div>
      <h1>UrlParameterページです</h1>
      <p>パラメーターは{id}です</p>
      <p>クエリのパラメーターは{query.get("name")}です</p>
    </div>
  );
};
