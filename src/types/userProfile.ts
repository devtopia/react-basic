export interface IUserProfile {
  id: number;
  name: string;
  email: string;
  address: string;
}
