import axios from "axios";
import React, { ReactElement } from "react";

export const UserList = (): ReactElement => {
  const onClickUsers = (): void => {
    axios
      .get("https://jsonplaceholder.typicode.com/users")
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => console.log(err));
  };
  const onClickUser1 = (): void => {
    axios
      .get("https://jsonplaceholder.typicode.com/users/3")
      .then((res) => {
        console.log(res.data);
      })
      .catch((err) => console.log(err));
  };
  return (
    <div>
      <button onClick={onClickUsers}>users</button>
      <button onClick={onClickUser1}>id=1 user</button>
    </div>
  );
};
