// import { Todos } from "Todos";
// import { UserList } from "UserList";
import { Users } from "Users";
import React from "react";
import ReactDOM from "react-dom/client";

// import { UserProfiles } from "./UserProfiles";
// import App from "./App";
import "./index.css";
import reportWebVitals from "./reportWebVitals";

// import { CssModules } from "./components/CssModules";
// import { Emotion } from "./components/Emotion";
// import { InlineStyle } from "./components/InlineStyle";
// import { StyledComponents } from "./components/StyledComponents";
// import { StyledJsx } from "./components/StyledJsx";
// import Top from "./Top";

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);
root.render(
  <React.StrictMode>
    <Users />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
