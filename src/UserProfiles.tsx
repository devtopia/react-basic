// import { HeaderOnly } from "components/templates/HeaderOnly";
import { UserProvider } from "providers/UserProvider";
import React, { ReactElement } from "react";
import { RecoilRoot } from "recoil";
import { UserProfileRouter } from "router/UserProfileRouter";

export const UserProfiles = (): ReactElement => {
  return (
    <RecoilRoot>
      <UserProvider>
        <UserProfileRouter />
      </UserProvider>
    </RecoilRoot>
  );
};
